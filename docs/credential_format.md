# Credential format

This section extends the information that the following documents provide about Self-Descriptions:

- the [Gaia-X Architecture Document](https://docs.gaia-x.eu/technical-committee/architecture-document/), which 
    - defines the Conceptual Model of entities that can have Self-Descriptions (*ServiceOffering*s, their *Provider*s, and the *Resource*s they aggregate), and
    - introduces Self-Descriptions and their lifecycle on a high level, and
- the [Gaia-X Trust Framework](https://docs.gaia-x.eu/policy-rules-committee/trust-framework/), which defines
    - the schema against which claims in Self-Descriptions shall be validated, e.g., whether mandatory attributes are present and whether attribute values have the correct datatypes, as well as
    - the trust anchors to which the identity of issuers of signatures must be traceable.

## Introduction
Gaia-X self-descriptions are tamper-proof presentations compiled from the holder's credentials including verified claims. The requester defines which self-description attributes it needs for the use case, and the holder provides them in a privacy-friendly manner. The Self-Description presetation documents that contain the description of Gaia-X entities such as *Participant* (including *Provider*), *Service Offering*, compositions of services, and the aggregation of *Resource*s by *Service Offering*s. 
The GAIA-X compliance services enable the participant to send non verified Self-Description attributes and after verification he sign the claims and issue idividual credetials to the provider. Afterward the issued credentials are used to offer verifiable Self-Description. 


## Self-Description Format and Structure

The serialization format of Verifiable Presentations, Verifiable Credentials and Credentials is JSON-LD and defined as follows.

Gaia-X Self-Description documents are **Verifiable Presentations** and **Verifiable Credentials** following the [W3C Verifiable Credentials Data Model](https://www.w3.org/TR/vc-data-model/).  
One such Verifiable Presentation contains one or more **Verifiable Credentials** with individual disclosed claims.  
Each Verifiable Credential contains signed **claims** about one or more *credential subjects*.

The Verifiable Credentials Data Model is defined as a vocabulary [W3C Verifiable Credentials Vocabulary](https://www.w3.org/2018/credentials/#%20schema%20context) based on the [W3C RDF Resource Description Framework](https://www.w3.org/TR/rdf11-concepts/) graph data model.  
Gaia-X uses the [JSON-LD](https://www.w3.org/TR/json-ld11/) serialization of Verifiable Credentials with linked data proof format [W3C Linked Data Proof](https://w3c.github.io/vc-data-integrity/).

In the most typical situation in Gaia-X, a Provider *P* presents to another Participant, or to a Federation Service acting on behalf of an actual or potential Participant, a Verifiable Presentation of *P* itself or of a ServiceOffering provided by *P*.  In this case, all Verifiable Credentials in the Verifiable Presentation contain claims about the same subject.  

Gaia-X also allows Verifiable Presentations to contain Verifiable Credentials that contain claims about multiple different subjects. Such a Verifiable Presentation may, e.g., have been produced by an Issuer who assessed multiple ServiceOfferings made by a Provider, or multiple Resources owned by a Provider, for conformance with some standard, in one bulk assessment.  

All objects exchanged and referenced in Gaia-X require Web-scalable identifiers, i.e., URIs.  It is recommended to use Decentralized Identifiers (DIDs) in any case to provide automated trust verification.

<details>
<summary>Example of the Self-Description</summary>

The following listing shows an example of a Self-Description.
For brevity, the claims about the credential subjects are placeholders.

```json linenums="1" title="verifiable_presentation.json" hl_lines="4 11 17"
{
  "@id": "http://example.org/verifiablePresentation/1",
  "@context": [ "https://www.w3.org/2018/credentials/v1" ],
  "@type": ["VerifiablePresentation"],
  "issuer": "https://example.org/issuers/565049",
  "issuanceDate": "2010-01-01T00:00:00Z",
  "expirationDate": "2033-01-01T00:00:00Z",
  "verifiableCredential": [ 
  {
    "@id": "http://example.org/verifiableCred/1",
    "@type": [ "VerifiableCredential" ],
    "issuer": "https://example.org/issuers/565049",
    "issuanceDate": "2010-01-01T00:00:00Z",
    "expirationDate": "2033-01-01T00:00:00Z",
    "credentialSubject": [
        {
          "@type": "gx:LegalPerson",
          "@id": "did:example:legalpersonABC",
          "@context": {"..."}
          "gx:registrationNumber": "...",
          ...
        },
        {
          ...
        }
      ],
      "proof": { ... }
    },
    {
      ...
    }
  ],
  "proof": { ... }
}
```

</details>

## Decentralized Identifiers

This section extends the [W3C Decentralized Identifiers](https://www.w3.org/TR/did-core/) to specify how it shall be applied in the scope of Gaia-X.

### Verification Methods

To ensure a Self-Description's integrity and authenticity, its claims must be cryptographically signed by the Participant that is issuing them.  
This is done to avoid tampering and to technically allow to check the origin of the claims.

The supported verification methods are described below.

#### JSON Web Key 2020

This section extends the specification from the [W3C JSON Web Key 2020](https://w3c-ccg.github.io/lds-jws2020/#json-web-key-2020).

A Verifiable Credential is *Gaia-X conformant* if the issuer of the Verifiable Credential itself has an verifiable identity coming from one of the [Trust Anchors](https://docs.gaia-x.eu/policy-rules-committee/trust-framework/latest/trust_anchors/).

Without a means to link the issuer's verification method to a Gaia-X Trust Anchor, the Gaia-X Compliance verification will fail.

To be able to assess the chain of trust, the `publicKeyJwk` property should include the [RFC7517 `x5c`](https://www.rfc-editor.org/rfc/rfc7517#section-4.7) (X.509 Certificate Chain) parameter or [RFC7517 `x5u`](https://www.rfc-editor.org/rfc/rfc7517#section-4.6) (X.509 URL) parameter.  
The `x5u` parameter should be resolvable to a `X509` `.crt`, `.pem`, `.der` or `.p7b` file which contains a valid Gaia-X Trust Anchor eligible for the signed claims.


<details>
<summary>Example of verificationMethod</summary>

```json title="verifiable_method.json"
{
  "..."
  "verificationMethod": [
    {
      "@context": "https://w3id.org/security/suites/jws-2020/v1",
      "id": "did:web:vc.gaia-x.eu:issuer#JsonWebKey2020-Ed25519",
      "controller": "did:web:vc.gaia-x.eu:issuer",
      "type": "JsonWebKey2020",
      "publicKeyJwk": {
        "crv": "Ed25519",
        "x": "ecBxqTm7YKx9lja3U-EzxMGLNz4xzBTm_iki3LUA-zc",
        "kty": "OKP",
        "x5u": "did:web:vc.gaia-x.eu:issuer#svc-fullchain"
      }
    }
  ]
}
```

</details>


## Use of Identifiers in Self-Descriptions

- 
Each of the following must have a different identifier:

* a Verifiable Presentation
* a Verifiable Credential inside a Verifiable Presentation
* the subject of a Verifiable Credential, i.e., the Conceptual Model entity that claims are made about.

In other words, the identity of a Self-Description (a Verifiable Presentation) is different from the identity of the thing that it describes (e.g., a *ServiceOffering*).

Self-Descriptions may reference other Self-Descriptions.  Consider, for example, a *ServiceOffering* that:

* is provided by a *Provider*,
* is a composition of other *ServiceOffering*s, or
* is an aggregation of *Resources*.

On the level of the graph of information about Gaia-X entities, which is formed by these claims, these relationships are between Gaia-X entities.  That is, the subject and object of such a claim are IDs of Gaia-X entities.  Thus, when the Self-Description *S_S* (a Verifiable Presentation) of a *ServiceOffering* states that the offering is made by some *Provider* *P*, the Self-Description *S_S* does not provide attestations of any claims about *P*.  Instead, these attestations must be provided in a dedicated Self-Description *S_P* of *P*, i.e., a Self-Description containing claims about the credential subject *P*.  The link from *S_S* to *S_P* is not explicit, but can be discovered by any Participant or Federation Service (e.g., a Federated Catalogue) that receives *S_S* as well as *S_P* and thus notices that their claims both refer to the same ID of *P*.

## Verifiable Credential and Verifiable Presentation

This section extends the [W3C Verifiable Credentials Data Model](https://www.w3.org/TR/vc-data-model/) to specify how it shall be applied in the scope of Gaia-X.

### Namespace Bindings and Contexts

On the level of the Verifiable Presentation and the Verifiable Credentials contained in the Verifiable Presentation, a Self-Description MUST adhere to the vocabulary of the Verifiable Credentials Data Model, i.e., use terms from the `https://www.w3.org/2018/credentials#` namespace.

To enable human authors of Self-Descriptions to write down these terms conveniently, they MAY, by using the `@context` keyword on the level of the Verifiable Presentation, e.g.:

- reference the [JSON-LD context](https://www.w3.org/TR/json-ld11/#the-context) provided by the Verifiable Credentials Data Model (https://www.w3.org/2018/credentials/v1) like in the initial example listing, or
- define their own context, which
    - defines the above namespace as the default vocabulary using the `@vocab` keyword, or
    - maps the above namespace to a designated prefix, e.g., `"cred"`.

Similarly, the claims about any credential subject MUST adhere to the vocabulary of the Gaia-X Self-Description Schemas published in the [Gaia-X Registry](https://registry.gaia-x.eu/), or to Federation-specific specializations of this vocabulary.

### Identifiers

The `@id` MUST be present and unique for a given `issuer`.

It is up to the `issuer` to decide if the `@id` is a resolvable [URI](https://www.rfc-editor.org/rfc/rfc3986) or not.

In order to enable reference to objects - Verifiable Credentials or credential subject - which are not under control of the same issuers, it is REQUIRED to specify an `@sri` [Subresource Integrity](https://www.w3.org/TR/SRI/) attribute to enable to verification of the integrity of the rerefenced object.

The `sri` attribute is computed by taking the hash of the referenced normalized JSON object.  
The JSON object are normalized following the JSON Canonicalization Scheme (JCS) defined in the [RFC 8785](https://www.rfc-editor.org/rfc/rfc8785).

<details>
<summary>Example of `sri` attribute</summary>

```json title="SRI attribute"
{
  "@id": "https://example.com/ABC",
  "sri": "sha256-b9a822666c3569a8ae80c897a1984f68bbdffa1f8141cacdb3f168b1c0b9aa36"
}
```

</details>

### Types

The `@type` MUST be present in Verifiable Presentation, Verifiable Credentials, and Credentials. The only expected values for the `@type` property are 

- `"VerifiablePresentation"` for a Verifiable Presentation
- `"VerifiableCredential"` for a Verifiable Credential

The expected values for the `@type` property of a credential subject are given by the taxonomy of classes defined in the [Gaia-X Trust Framework](https://docs.gaia-x.eu/policy-rules-committee/trust-framework/), having the superclasses `Participant`, `ServiceOffering` and `Resource`.

A Federation MAY define additional subclasses of these by further shapes hosted in its Catalogue(s).
In future, Gaia-X and federations MAY also define additional, more specific credential types.

#### Schema Validation

A Schema for Self-Descriptions, to be used as the vocabulary of the claims about credential subjects, MUST be available in the form of SHACL shapes (cf. the [W3C Shapes Constraint Language SHACL](https://www.w3.org/TR/shacl/)) in the Gaia-X Registry or in the Catalogue of a Federation.  
At any point where Self-Descriptions are created or received, a certain set of SHACL shapes is known, which forms a *shapes graph*.
A Self-Description forms a *data graph*.  For compliance with Gaia-X and/or a specific Federation, this *data graph* MUST be validated against the given *shapes graph* [according to the SHACL specification](https://www.w3.org/TR/shacl/#validation-definition).

### Issuers

The `issuer` property MUST be present in Verifiable Credential and Verifiable Presentation. The value of the `issuer` property must be a resolvable URI.  

The supported schemes for `issuer`'s URI are:

- `https`
- `did`. The supported [DID methods](https://w3c.github.io/did-spec-registries/#did-methods) are:
    - `web`

### IssuanceData

The `issuanceDate` is MANDATORY for Verifiable Credential and Verifiable Presentation.

### ExpirationDate

The `expirationDate` is RECOMMENDED for Verifiable Credential and Verifiable Presentation.

### Verifiable Credential

If the `@type` is `"VerifiablePresentation"`, the property `verifiableCredential` MUST be defined. The value of `verifiableCredential` can be a Verifiable Credential or an array of Verifiable Credentials. A Verifiable Credential MUST have 

- an [`@id`](#identifiers),
- an [`issuer`](#issuers),
- a [`@type`](#types), and
- a ['credentialSubject](#Credential_Subject)

### Credential Subject

The `credentialSubject` can be an object or array of objects, containing claims.

The claims about one Gaia-X entity MAY be spread over multiple Credentials and their subjects.

Each credential subject MUST have an [`@id`](#identifiers).

A credential subject MAY be described *by value*, i.e., by stating one or more claims about it in place.
In this case, it MUST have a [`@type`](#types) as specified below.

Alternatively, a credential subject MAY be described *by reference*.
In this case, the `@id` MUST be resolvable to an RDF resource that has the same `@id`, a `@type`, and one or more claims. See [Identifiers](#identifiers) section for more details.

The value of the `@type` property dictates the vocabulary available in the [Trust Framework](https://docs.gaia-x.eu/policy-rules-committee/trust-framework/) document for the definition of claims about the credential subject. E.g., `LegalPerson`, `ServiceOffering`, `DataResource`, ...

<details>
<summary>Example of type in credentialSubject</summary>

```json
{
  "id":"did:example:cred2",
  "@context": {
    "gx": "https://registry.gaia-x.eu/22.04/schema/gaia-x/participant#",
    "xsd": "http://www.w3.org/2001/XMLSchema#"
  },
  "@type":"gx:LegalPerson",
  "gx:registrationNumber":{
    "@value":"DEK1234R.HRB123456",
    "@type":"xsd:string"
  },
  "gx:headquarterAddress": {
    "gx:countryCode": {
      "@value": "DE",
      "@type": "xsd:string"
    }
  },
  "gx:legalAddress": {
    "gx:countryCode": {
      "@value":"DE",
      "@type":"xsd:string"
    }
  }
}
```

</details>

### Proofs (Signatures)

If the `@type` is `"VerifiableCredential"`, then at least one `proof` MUST be included.

The supported `@type` of proof is [`JsonWebSignature2020`](https://w3c-ccg.github.io/lds-jws2020/) using `JsonWebKey2020` verification method.

Before signing, the Self-Description must be normalised using the Universal RDF Dataset Canonicalization Algorithm 2015 ([URDNA2015](https://json-ld.github.io/rdf-dataset-canonicalization/spec/)). Then, the normalised Self-Description must be hashed with `SHA256`. Lastly, the hash (`SHA256(normalisedSelfDescription)`) is signed resulting in a `jws`. The signature must use the JWS Unencoded Payload Option ([RFC7797](https://www.rfc-editor.org/rfc/rfc7797)) with detached payload.


## Gaia-X compliance input/output

The Gaia-X Compliance service

```mermaid
flowchart LR

in[Verifiable Presentation]
compliance[Gaia-X Compliance]
out[Verifiable Credential]

in --> |input|compliance --> |output|out
```

## Input

The input of the Gaia-X Compliance service is a `Verifiable Presentation` that might contain:

- a Verifiable Credential [by value](#byvalue)
- a Verifiable Credential [by reference](#byreference)

Such a Verifiable Credential may or may not be covered by the [Gaia-X Compliance rules](http://docs.gaia-x.eu/policy-rules-committee/trust-framework/).

The following example contains fake, placeholder attributes for `Participant` and `ServiceOffering`, which are not valid against the Gaia-X Self-Description Schema.

<details>
<summary>Example of Compliance input</summary>

```json title="compliance_input.json"
{
  "@id": "http://example.edu/verifiablePresentation/1",
  "@context": [
    "https://www.w3.org/2018/credentials/v1"
  ],
  "type": [
    "VerifiablePresentation"
  ],
  "issuer": "https://example.edu/issuers/565048",
  "issuanceDate": "2010-01-01T00:00:00Z",
  "verifiableCredential": [
    {
      "@id": "http://example.edu/VerifiableCred/1"
    },
    {
      "@id": "http://example.edu/verifiableCred/2",
      "type": [
        "VerifiableCredential"
      ],
      "issuer": "https://example.edu/issuers/565049",
      "issuanceDate": "2010-01-01T00:00:00Z",
      "credentialSubject": [
        {
          "@id": "did:example:cred1",
          "type": "Participant",
          "claim1": {
            "@value": "foo",
            "@type": "xsd:string"
          },
          "claim2": {
            "@value": "bar",
            "@type": "xsd:string"
          }
        },
        {
          "id": "did:example:cred2",
          "type": "ServiceOffering",
          "claim1": {
            "@value": "foo",
            "@type": "xsd:string"
          },
          "claim2": {
            "@value": "bar",
            "@type": "xsd:string"
          }
        }
      ],
      "proof": {
        "type": "JsonWebSignature2020",
        "created": "2022-02-25T14:58:43Z",
        "verificationMethod": "https://example.edu/issuers/565049#key-1",
        "proofPurpose": "assertionMethod",
        "jws": "z2iiwEyyGcqwLPMQDXnjEdQU4zGzWs6cgjrmXAM4LRcfXni1PpZ44EBuU6o2EnkXr4uNMVJcMbaYTLBg3WYLbev3S"
      }
    }
  ]
}
```

</details>

## Output

The output of the Gaia-X Compliance service is a `VerifiableCredential` containing the `id` and `hash` of the compliant `VerifiableCredential` from the input.


<details>
<summary>Example of Compliance output</summary>


```json title="compliance_output.json"
{
  "id": "http://example.edu/complianceResult/1",
  "type": [
    "VerifiableCredential"
  ],
  "issuer": "https://example.edu/issuers/565050",
  "issuanceDate": "2010-01-01T00:00:00Z",
  "expirationDate": "2010-03-01T00:00:00Z",
  "credentialSubject": [
    {
      "type": "gaiaxCompliance",
      "id": "http://example.edu/verifiableCred/1",
      "hash": "0f5ced733003d11798006639a5200db78206e43c85aa0386d7909c3e6c8ed535"
    },
    {
      "type": "gaiaxCompliance",
      "id": "http://example.edu/verifiableCred/2",
      "hash": {
        "@value": "0f5ced733003d11798006639a5200db78206e43c85aa123456789789123456798",
        "@type": "xsd:string",
        "@checksumtype": "SHA-256"
      }
    }
  ],
  "proof": {
    "type": "JsonWebSignature2020",
    "created": "2022-06-12T19:38:26.853Z",
    "proofPurpose": "assertionMethod",
    "jws": "z2iiwEyyGcqwLPMQDXnjEdQU4zGzWs6cgjrmXAM4LRcfXni1PpZ44EBuU6o2EnkXr4uNMVJcMbaYTLBg3WYLbev3S",
    "verificationMethod": "did:web:compliance.gaia-x.eu"
  }
}
```

</details>
