# Gaia-X TC WG Identity & Access Management - Meeting notes for 12/07/2022

## Minutes

### Closed issues and decisions

* [Issue 13 : Do we need to promote a policy langage in the specification ?](https://gitlab.com/gaia-x/technical-committee/federation-services/iam/-/issues/13) : Yes, we promote **REGO** as policy language.


### Open issues and questions :

* [Issue 15 : IAM Vocabulary](https://gitlab.com/gaia-x/technical-committee/federation-services/iam/-/issues/15) : See comments in the issue.


### Discussion and proposals
* [OpenID Connect compatibility] About SSI - OpenID Connect bridge, Berthold sent us a [link](https://gitlab.com/gaia-x/data-infrastructure-federation-services/authenticationauthorization/-/tree/main) to the implementation made by ECO (compatible with Keycloak).

### Demonstration

First part of the demonstration of [TSA services](https://gitlab.com/gaia-x/data-infrastructure-federation-services/tsa). We see :
* The Policy engine part
* The DID resolver part
* The Signer part

We will continue the demonstration next week following by a demonstration of the login part based on OpenID / Keycloak.